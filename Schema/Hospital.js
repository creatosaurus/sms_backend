const mongoose = require('mongoose')
const schema = mongoose.Schema

const Hospital = new schema({
    email: {
        type: String,
        trim: true,
        lowercase: true,
        unique: true,
        required: 'Email address is required',
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address']
    },
    name: {
        type: String,
        trim: true,
        unique: true,
    },
    password: {
        type: String,
        trim: true
    },
    mobile: {
        type: Number
    },
    address: {
        type: String,
    }
}, { timestamps: true })

module.exports = mongoose.model('Hospital', Hospital)