const mongoose = require('mongoose')
const schema = mongoose.Schema

const Patient = new schema({
    hospitalId: String,
    email: {
        type: String,
        trim: true,
        lowercase: true,
    },
    name: {
        type: String,
        trim: true,
        unique: true,
    },
    mobile: Number
}, { timestamps: true })

module.exports = mongoose.model('Patient', Patient)