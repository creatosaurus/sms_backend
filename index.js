const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')
const { sendMessage } = require('./Functions/Twillio')
const MessagingResponse = require('twilio').twiml.MessagingResponse;

// declare the app
const app = express()

// connect to the data base
connectToDataBase = async () => {
    try {
        await mongoose.connect('mongodb+srv://Roshan:3BrOr8xsdbwOlqGw@cluster0.hetj0.mongodb.net/sms?retryWrites=true&w=majority', { useNewUrlParser: true, useUnifiedTopology: true })
        console.log("connected to data base")
    } catch (error) {
        process.exit(1);
    }
}

connectToDataBase()

// middle ware
app.use(cors())
app.use(express.urlencoded({ extended: true }))
app.use(express.json())

app.post('/sms', (req, res) => {
    console.log("i am called")
    const twiml = new MessagingResponse();

    const message = twiml.message();
    message.body('The Robots are coming! Head for the hills!');
  
    res.writeHead(200, {'Content-Type': 'text/xml'});
    res.end(twiml.toString());
});

app.get('/', (req, res) => {
    sendMessage()
    res.send("done")
})

app.use('/hospital', require('./Routes/Hospital'))
app.use('/patient', require('./Routes/Patient'))
app.use('/survey', require('./Routes/Survey'))


app.listen(process.env.PORT || 4000 , () => {
    console.log("server listening on port 4000")
})
