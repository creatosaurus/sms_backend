const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const hospital = require('../Schema/Hospital')

router.post('/signup', async (req, res) => {
    try {
        if (req.body.password !== null) {
            // generate the hash and assign to the password in body
            const hash = await bcrypt.hash(req.body.password, 10)
            req.body.password = hash
        }

        // check the user exist in the data base or not
        const userExist = await hospital.findOne({ email: req.body.email })
        if (userExist !== null) {
            return res.status(409).json({
                error: "already registered",
            })
        }

        // create the new user in data base and generate the token
        const data = await hospital.create(req.body)
        const token = jwt.sign({
            data: data.email,
            id: data._id
        }, 'secret', { expiresIn: '24h' });

        return res.status(200).json({
            message: "authentication succesfull",
            token
        })
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            error: error
        })
    }
})

router.post('/login', async (req, res) => {
    try {
        const data = await hospital.findOne({ facebookId: req.body.facebookId })
        if (data === null) return res.status(404).json({ error: 'user not found please sign up' })

        // compare the hash password and requested password
        const result = await bcrypt.compare(req.body.password, data.password)
        if (!result) {
            return res.status(404).json({
                error: 'authentication failed'
            })
        } else {
            // generate the token
            const token = jwt.sign({
                data: data.email,
                id: data._id
            }, 'secret', { expiresIn: '24h' });

            return res.status(200).json({
                message: "authentication succesfull",
                token
            })
        }
    } catch (error) {
        return res.status(500).json({
            error: error
        })
    }
})

module.exports = router