const express = require('express')
const router = express.Router()
const patient = require('../Schema/Patient')

router.get('/:id', async (req, res) => {
    try {
        const patientData = await patient.find({ hospitalId: req.params.id })
        return res.status(200).json({
            message: "success",
            data: patientData
        })
    } catch (error) {
        return res.status(500).json({
            error: error
        })
    }
})

router.post('/create', async (req, res) => {
    try {
        const patientData = await patient.create(req.body)
        return res.status(201).json({
            message: "success",
            data: patientData
        })
    } catch (error) {
        return res.status(500).json({
            error: error
        })
    }
})

module.exports = router